import React from 'react';
import ReactDOM from 'react-dom';

class Item extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataItems: []
        };
    }
    loadData() {
        let basicUrl = "https://swapi.co/api/";
        let url = "";
        
        switch(this.props.match.path) {
            case "/characters/":
              url = basicUrl + "people/";
              break;
            case "/planets/":
                url = basicUrl + "planets/";
              break;
            case "/starships/":
                url = basicUrl + "starships/";
              break;
            default:
              // code block
        }

        const ourHeaders = new Headers();
        ourHeaders.append("Accept", "*/*");
        // ourHeaders.append("Authorization", "Bearer " + localStorage.getItem("AccessToken"))
        ourHeaders.append("Content-Type", "application/json charset=utf-8");

        fetch(url, {
                method: "get",
                headers: ourHeaders,
            })
            .then(response => response.json())
            .then(data => {

                this.setState({dataItems: data.results })
        });
    }
    componentDidMount() {
        this.loadData();
    }
    createdAtHelper(date) {
        var _date = new Date(date);
        _date = _date.toLocaleDateString();
        return _date;
    }
    render () {
        const data = this.state.dataItems;
        if (data.length > 0) {
            return (
                <div className="window">DATA</div>
            );
        } else {
            return (
                <div className="no-result">Loading ...</div>
            );
        }
    }
}

export default Item;
