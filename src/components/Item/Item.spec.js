import React from 'react';
import { mount } from 'enzyme';

import Item from './Item';

describe('Item component', () => {
    it('should match snapshot', () => {
        expect(mount(<Item />)).toMatchSnapshot();
    });
});
