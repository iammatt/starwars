import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Item from './Item';

storiesOf('Item', module)
    .add('with title', withInfo()(() => (
        <Item title="Item title" />
    )));
