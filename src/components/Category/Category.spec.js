import React from 'react';
import { mount } from 'enzyme';

import Category from './Category';

describe('Category component', () => {
    it('should match snapshot', () => {
        expect(mount(<Category />)).toMatchSnapshot();
    });
});
