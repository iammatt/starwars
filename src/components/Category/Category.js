import React from 'react';
import ReactDOM from 'react-dom';
import './Category.css';
import { DH_CHECK_P_NOT_PRIME } from 'constants';

class Category extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemLoaded: false,
            dataItems: [],
            itemDetails: ""
        };

        this.loadDetails = this.loadDetails.bind(this);
    }
    loadData() {
        let basicUrl = "https://swapi.co/api/";
        let url = "";
        
        switch(this.props.match.path) {
            case "/characters/":
              url = basicUrl + "people/";
              break;
            case "/planets/":
                url = basicUrl + "planets/";
              break;
            case "/starships/":
                url = basicUrl + "starships/";
              break;
            default:
              url = basicUrl
        }

        const ourHeaders = new Headers();
        ourHeaders.append("Accept", "*/*");
        ourHeaders.append("Content-Type", "application/json charset=utf-8");

        fetch(url, {
                method: "get",
                headers: ourHeaders,
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({dataItems: data.results })
        });
    }
    componentDidMount() {
        this.loadData();
    }
    createdAtHelper(date) {
        var _date = new Date(date);
        _date = _date.toLocaleDateString();
        return _date;
    }
    loadDetails(item) {
        const ourHeaders = new Headers();
        ourHeaders.append("Accept", "*/*");
        ourHeaders.append("Content-Type", "application/json charset=utf-8");

        fetch(item.target.dataset.path, {
                method: "get",
                headers: ourHeaders,
            })
            .then(response => response.json())
            .then(data => {
                let template = "";

                console.log(data);

                if (data.population !== undefined) {
                    template = this.planetTemplate(data);
                } else if (data.MGLT !== undefined) {
                    template = this.starshipTemplate(data);
                } else {
                    template = this.peopleTemplate(data);
                }

                document.body.innerHTML += `<div class='window'>${template}</div>`;

                this.setState({ itemLoaded: true });

                document.getElementById("close-window").addEventListener("click", function(e) {
                    window.location.reload();
                });
        });
    }
    starshipTemplate(ship) {
        let tpl = `
            <div class="window-title">${ship.name}</div>
            <div class="window-content">
                <ul>
                    <li>MGLT: ${ship.MGLT}</li>
                    <li>Cargo capacity: ${ship.cargo_capacity}</li>
                    <li>Consumables: ${ship.consumables}</li>
                    <li>Cost in credits: ${ship.cost_in_credits}</li>
                    <li>Crew: ${ship.crew}</li>
                    <li>Hyperdrive rating: ${ship.hyperdrive_rating}</li>
                    <li>Length: ${ship.length}</li>
                    <li>Manufacturer: ${ship.manufacturer}</li>
                    <li>Model: ${ship.model}</li>
                    <li>Passengers: ${ship.passengers}</li>
                    <li>Starship class: ${ship.starship_class}</li>
                </ul>
            </div>
            <div class="window-footer">
                <a href="#" id="close-window">Close</a>
            </div>
        `;
        return tpl;
    }
    planetTemplate(planet) {
        let tpl = `
            <div class="window-title">${planet.name}</div>
            <div class="window-content">
                <ul>
                    <li>Climate: ${planet.climate}</li>
                    <li>Diameter: ${planet.diameter}</li>
                    <li>Gravity: ${planet.gravity}</li>
                    <li>Orbital period: ${planet.orbital_period}</li>
                    <li>Population: ${planet.population}</li>
                    <li>Rotation period: ${planet.rotation_period}</li>
                    <li>Surface water: ${planet.surface_water}</li>
                    <li>Terrain: ${planet.terrain}</li>
                </ul>
            </div>
            <div class="window-footer">
                <a href="#" id="close-window">Close</a>
            </div>
        `;
        return tpl;
    }
    peopleTemplate(person) {
        let tpl = `
            <div class="window-title">${person.name}</div>
            <div class="window-content">
                <ul>
                    <li>Skin color: ${person.skin_color}</li>
                    <li>Mass: ${person.mass}</li>
                    <li>Height: ${person.height}</li>
                    <li>Hair color: ${person.hair_color}</li>
                    <li>Gender: ${person.gender}</li>
                    <li>Eye color: ${person.eye_color}</li>
                    <li>Birth year: ${person.birth_year}</li>
                </ul>
            </div>
            <div class="window-footer">
                <a href="#" id="close-window">Close</a>
            </div>
        `;
        return tpl;
    }
    render () {
        const data = this.state.dataItems;
        if (data.length > 0) {
            return (
                <div className="items">
                    {data.map((item, index) =>
                        <div key={index} className="item">
                            <div className="item__title">{item.name}</div>
                            <div className="item__details">Created at: {this.createdAtHelper(item.created)}</div>
                            <button className="button" data-path={item.url} onClick={this.loadDetails}>More...</button>
                        </div>
                    )}
                </div>
            );
        } else {
            return (
                <div className="no-result">Loading ...</div>
            );
        }
    }
}

export default Category;
