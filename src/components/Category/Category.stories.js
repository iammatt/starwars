import React from 'react';
import { storiesOf } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import Category from './Category';

storiesOf('Category', module)
    .add('with title', withInfo()(() => (
        <Category title="Category title" />
    )));
