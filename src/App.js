import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Category from './components/Category';
import Item from './components/Item';
import './App.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="categories">
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/characters/">Characters</Link>
              </li>
              <li>
                <Link to="/planets/">Planets</Link>
              </li>
              <li>
                <Link to="/starships/">Starships</Link>
              </li>
            </ul>  
          </nav>
          <Route path="/characters/" component={Category} />
          <Route path="/planets/" component={Category} />
          <Route path="/starships/" component={Category} />
        </div>
      </Router>
    );
  }
}

export default App;
